drop view if exists v_wordpress_vendor CASCADE;
create view v_wordpress_vendor as
select user_id as vendor_id, meta_value as vendor_name from wordpress_wp_usermeta where meta_key in ( 'store_name');




/*
create or replace view v_wp_usermeta as
select umeta_id, user_id, replace(meta_key, char(195), '') as meta_key, replace(meta_value, char(195), 'e') as meta_value
from wp_usermeta;


create or replace view v_wp_posts as
select
ID,post_author,post_date,post_date_gmt,post_content
,replace(replace(replace(post_title, char(195), ''), char(226), 'EUR'), char(194),'.') as post_title
,post_excerpt,post_status,comment_status,ping_status,post_password,post_name,to_ping,pinged,post_modified,post_modified_gmt,post_content_filtered,post_parent,guid,menu_order,post_type,post_mime_type,comment_count
from wp_posts;

create view v_matomo_log_action as
select idaction , replace(replace(replace(name, char(195), ''), char(226), 'EUR'), char(194),'.') as name, hash, type, url_prefix
from matomo_log_action 
*/
