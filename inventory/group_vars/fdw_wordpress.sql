drop foreign table if exists wordpress_wp_actionscheduler_actions CASCADE; create FOREIGN TABLE wordpress_wp_actionscheduler_actions("action_id" bigint NOT NULL
	,"hook" varchar(191) NOT NULL
	,"status" varchar(20) NOT NULL
	,"scheduled_date_gmt" timestamp NOT NULL
	,"scheduled_date_local" timestamp NOT NULL
	,"args" varchar(191)
	,"schedule" varchar(4000)
	,"group_id" bigint NOT NULL
	,"attempts" int NOT NULL
	,"last_attempt_gmt" timestamp NOT NULL
	,"last_attempt_local" timestamp NOT NULL
	,"claim_id" bigint NOT NULL
	,"extended_args" varchar(8000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_actionscheduler_actions');
drop foreign table if exists wordpress_wp_actionscheduler_claims CASCADE; create FOREIGN TABLE wordpress_wp_actionscheduler_claims("claim_id" bigint NOT NULL
	,"date_created_gmt" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_actionscheduler_claims');
drop foreign table if exists wordpress_wp_actionscheduler_groups CASCADE; create FOREIGN TABLE wordpress_wp_actionscheduler_groups("group_id" bigint NOT NULL
	,"slug" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_actionscheduler_groups');
drop foreign table if exists wordpress_wp_actionscheduler_logs CASCADE; create FOREIGN TABLE wordpress_wp_actionscheduler_logs("log_id" bigint NOT NULL
	,"action_id" bigint NOT NULL
	,"message" text NOT NULL
	,"log_date_gmt" timestamp NOT NULL
	,"log_date_local" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_actionscheduler_logs');
drop foreign table if exists wordpress_wp_commentmeta CASCADE; create FOREIGN TABLE wordpress_wp_commentmeta("meta_id" bigint NOT NULL
	,"comment_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_commentmeta');
drop foreign table if exists wordpress_wp_comments CASCADE; create FOREIGN TABLE wordpress_wp_comments("comment_ID" bigint NOT NULL
	,"comment_post_ID" bigint NOT NULL
	,"comment_author" varchar(4000) NOT NULL
	,"comment_author_email" varchar(100) NOT NULL
	,"comment_author_url" varchar(200) NOT NULL
	,"comment_author_IP" varchar(100) NOT NULL
	,"comment_date" timestamp NOT NULL
	,"comment_date_gmt" timestamp NOT NULL
	,"comment_content" text NOT NULL
	,"comment_karma" int NOT NULL
	,"comment_approved" varchar(20) NOT NULL
	,"comment_agent" varchar(255) NOT NULL
	,"comment_type" varchar(20) NOT NULL
	,"comment_parent" bigint NOT NULL
	,"user_id" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_comments');
drop foreign table if exists wordpress_wp_e_submissions CASCADE; create FOREIGN TABLE wordpress_wp_e_submissions("id" bigint NOT NULL
	,"type" varchar(255)
	,"hash_id" varchar(255) NOT NULL
	,"main_meta_id" bigint NOT NULL
	,"post_id" bigint NOT NULL
	,"referer" varchar(500) NOT NULL
	,"referer_title" varchar(300)
	,"element_id" varchar(255) NOT NULL
	,"form_name" varchar(255) NOT NULL
	,"campaign_id" bigint NOT NULL
	,"user_id" bigint
	,"user_ip" varchar(46) NOT NULL
	,"user_agent" text NOT NULL
	,"actions_count" int
	,"actions_succeeded_count" int
	,"status" varchar(20) NOT NULL
	,"is_read" int NOT NULL
	,"meta" text
	,"created_at_gmt" timestamp NOT NULL
	,"updated_at_gmt" timestamp NOT NULL
	,"created_at" timestamp NOT NULL
	,"updated_at" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_e_submissions');
drop foreign table if exists wordpress_wp_e_submissions_actions_log CASCADE; create FOREIGN TABLE wordpress_wp_e_submissions_actions_log("id" bigint NOT NULL
	,"submission_id" bigint NOT NULL
	,"action_name" varchar(255) NOT NULL
	,"action_label" varchar(255)
	,"status" varchar(20) NOT NULL
	,"log" text
	,"created_at_gmt" timestamp NOT NULL
	,"updated_at_gmt" timestamp NOT NULL
	,"created_at" timestamp NOT NULL
	,"updated_at" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_e_submissions_actions_log');
drop foreign table if exists wordpress_wp_e_submissions_values CASCADE; create FOREIGN TABLE wordpress_wp_e_submissions_values("id" bigint NOT NULL
	,"submission_id" bigint NOT NULL
	,"key" varchar(255)
	,"value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_e_submissions_values');
drop foreign table if exists wordpress_wp_links CASCADE; create FOREIGN TABLE wordpress_wp_links("link_id" bigint NOT NULL
	,"link_url" varchar(255) NOT NULL
	,"link_name" varchar(255) NOT NULL
	,"link_image" varchar(255) NOT NULL
	,"link_target" varchar(25) NOT NULL
	,"link_description" varchar(255) NOT NULL
	,"link_visible" varchar(20) NOT NULL
	,"link_owner" bigint NOT NULL
	,"link_rating" int NOT NULL
	,"link_updated" timestamp NOT NULL
	,"link_rel" varchar(255) NOT NULL
	,"link_notes" varchar(4000) NOT NULL
	,"link_rss" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_links');
drop foreign table if exists wordpress_wp_mailerlite_checkouts CASCADE; create FOREIGN TABLE wordpress_wp_mailerlite_checkouts("id" int NOT NULL
	,"time" timestamp NOT NULL
	,"checkout_id" varchar(55) NOT NULL
	,"email" text NOT NULL
	,"cart_content" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_mailerlite_checkouts');
drop foreign table if exists wordpress_wp_ml_data CASCADE; create FOREIGN TABLE wordpress_wp_ml_data("data_name" varchar(45) NOT NULL
	,"data_value" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_ml_data');
drop foreign table if exists wordpress_wp_options CASCADE; create FOREIGN TABLE wordpress_wp_options("option_id" bigint NOT NULL
	,"option_name" varchar(191) NOT NULL
	,"option_value" varchar(4000) NOT NULL
	,"autoload" varchar(20) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_options');
drop foreign table if exists wordpress_wp_postmeta CASCADE; create FOREIGN TABLE wordpress_wp_postmeta("meta_id" bigint NOT NULL
	,"post_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_postmeta');
drop foreign table if exists wordpress_wp_posts CASCADE; create FOREIGN TABLE wordpress_wp_posts("id" bigint NOT NULL
	,"post_author" bigint NOT NULL
	,"post_date" timestamp NOT NULL
	,"post_date_gmt" timestamp NOT NULL
	,"post_content" varchar(4000) NOT NULL
	,"post_title" text NOT NULL
	,"post_excerpt" text NOT NULL
	,"post_status" varchar(20) NOT NULL
	,"comment_status" varchar(20) NOT NULL
	,"ping_status" varchar(20) NOT NULL
	,"post_password" varchar(255) NOT NULL
	,"post_name" varchar(200) NOT NULL
	,"to_ping" text NOT NULL
	,"pinged" text NOT NULL
	,"post_modified" timestamp NOT NULL
	,"post_modified_gmt" timestamp NOT NULL
	,"post_content_filtered" varchar(4000) NOT NULL
	,"post_parent" bigint NOT NULL
	,"guid" varchar(255) NOT NULL
	,"menu_order" int NOT NULL
	,"post_type" varchar(20) NOT NULL
	,"post_mime_type" varchar(100) NOT NULL
	,"comment_count" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_posts');
drop foreign table if exists wordpress_wp_revslider_css CASCADE; create FOREIGN TABLE wordpress_wp_revslider_css("id" int NOT NULL
	,"handle" text NOT NULL
	,"settings" varchar(4000)
	,"hover" varchar(4000)
	,"advanced" varchar(4000)
	,"params" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_css');
drop foreign table if exists wordpress_wp_revslider_css_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_css_bkp("id" int NOT NULL
	,"handle" text NOT NULL
	,"settings" varchar(4000)
	,"hover" varchar(4000)
	,"advanced" varchar(4000)
	,"params" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_css_bkp');
drop foreign table if exists wordpress_wp_revslider_layer_animations CASCADE; create FOREIGN TABLE wordpress_wp_revslider_layer_animations("id" int NOT NULL
	,"handle" text NOT NULL
	,"params" text NOT NULL
	,"settings" text)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_layer_animations');
drop foreign table if exists wordpress_wp_revslider_layer_animations_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_layer_animations_bkp("id" int NOT NULL
	,"handle" text NOT NULL
	,"params" text NOT NULL
	,"settings" text)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_layer_animations_bkp');
drop foreign table if exists wordpress_wp_revslider_navigations CASCADE; create FOREIGN TABLE wordpress_wp_revslider_navigations("id" int NOT NULL
	,"name" varchar(191) NOT NULL
	,"handle" varchar(191) NOT NULL
	,"type" varchar(191) NOT NULL
	,"css" varchar(4000) NOT NULL
	,"markup" varchar(4000) NOT NULL
	,"settings" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_navigations');
drop foreign table if exists wordpress_wp_revslider_navigations_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_navigations_bkp("id" int NOT NULL
	,"name" varchar(191) NOT NULL
	,"handle" varchar(191) NOT NULL
	,"type" varchar(191) NOT NULL
	,"css" varchar(4000) NOT NULL
	,"markup" varchar(4000) NOT NULL
	,"settings" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_navigations_bkp');
drop foreign table if exists wordpress_wp_revslider_sliders CASCADE; create FOREIGN TABLE wordpress_wp_revslider_sliders("id" int NOT NULL
	,"title" varchar(4000) NOT NULL
	,"alias" varchar(4000)
	,"params" varchar(4000) NOT NULL
	,"settings" text
	,"type" varchar(191) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_sliders');
drop foreign table if exists wordpress_wp_revslider_sliders_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_sliders_bkp("id" int NOT NULL
	,"title" varchar(4000) NOT NULL
	,"alias" varchar(4000)
	,"params" varchar(4000) NOT NULL
	,"settings" text
	,"type" varchar(191) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_sliders_bkp');
drop foreign table if exists wordpress_wp_revslider_slides CASCADE; create FOREIGN TABLE wordpress_wp_revslider_slides("id" int NOT NULL
	,"slider_id" int NOT NULL
	,"slide_order" int NOT NULL
	,"params" varchar(4000) NOT NULL
	,"layers" varchar(4000) NOT NULL
	,"settings" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_slides');
drop foreign table if exists wordpress_wp_revslider_slides_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_slides_bkp("id" int NOT NULL
	,"slider_id" int NOT NULL
	,"slide_order" int NOT NULL
	,"params" varchar(4000) NOT NULL
	,"layers" varchar(4000) NOT NULL
	,"settings" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_slides_bkp');
drop foreign table if exists wordpress_wp_revslider_static_slides CASCADE; create FOREIGN TABLE wordpress_wp_revslider_static_slides("id" int NOT NULL
	,"slider_id" int NOT NULL
	,"params" varchar(4000) NOT NULL
	,"layers" varchar(4000) NOT NULL
	,"settings" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_static_slides');
drop foreign table if exists wordpress_wp_revslider_static_slides_bkp CASCADE; create FOREIGN TABLE wordpress_wp_revslider_static_slides_bkp("id" int NOT NULL
	,"slider_id" int NOT NULL
	,"params" varchar(4000) NOT NULL
	,"layers" varchar(4000) NOT NULL
	,"settings" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_revslider_static_slides_bkp');
drop foreign table if exists wordpress_wp_term_relationships CASCADE; create FOREIGN TABLE wordpress_wp_term_relationships("object_id" bigint NOT NULL
	,"term_taxonomy_id" bigint NOT NULL
	,"term_order" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_term_relationships');
drop foreign table if exists wordpress_wp_term_taxonomy CASCADE; create FOREIGN TABLE wordpress_wp_term_taxonomy("term_taxonomy_id" bigint NOT NULL
	,"term_id" bigint NOT NULL
	,"taxonomy" varchar(32) NOT NULL
	,"description" varchar(4000) NOT NULL
	,"parent" bigint NOT NULL
	,"count" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_term_taxonomy');
drop foreign table if exists wordpress_wp_termmeta CASCADE; create FOREIGN TABLE wordpress_wp_termmeta("meta_id" bigint NOT NULL
	,"term_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_termmeta');
drop foreign table if exists wordpress_wp_terms CASCADE; create FOREIGN TABLE wordpress_wp_terms("term_id" bigint NOT NULL
	,"name" varchar(200) NOT NULL
	,"slug" varchar(200) NOT NULL
	,"term_group" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_terms');
drop foreign table if exists wordpress_wp_usermeta CASCADE; create FOREIGN TABLE wordpress_wp_usermeta("umeta_id" bigint NOT NULL
	,"user_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_usermeta');
drop foreign table if exists wordpress_wp_users CASCADE; create FOREIGN TABLE wordpress_wp_users("ID" bigint NOT NULL
	,"user_login" varchar(60) NOT NULL
	,"user_pass" varchar(255) NOT NULL
	,"user_nicename" varchar(250) NOT NULL
	,"user_email" varchar(100) NOT NULL
	,"user_url" varchar(100) NOT NULL
	,"user_registered" timestamp NOT NULL
	,"user_activation_key" varchar(255) NOT NULL
	,"user_status" int NOT NULL
	,"display_name" varchar(250) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_users');
drop foreign table if exists wordpress_wp_wc_admin_note_actions CASCADE; create FOREIGN TABLE wordpress_wp_wc_admin_note_actions("action_id" bigint NOT NULL
	,"note_id" bigint NOT NULL
	,"name" varchar(255) NOT NULL
	,"label" varchar(255) NOT NULL
	,"query" varchar(4000) NOT NULL
	,"status" varchar(255) NOT NULL
	,"is_primary" int NOT NULL
	,"actioned_text" varchar(255) NOT NULL
	,"nonce_action" varchar(255)
	,"nonce_name" varchar(255))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_admin_note_actions');
drop foreign table if exists wordpress_wp_wc_admin_notes CASCADE; create FOREIGN TABLE wordpress_wp_wc_admin_notes("note_id" bigint NOT NULL
	,"name" varchar(255) NOT NULL
	,"type" varchar(20) NOT NULL
	,"locale" varchar(20) NOT NULL
	,"title" varchar(4000) NOT NULL
	,"content" varchar(4000) NOT NULL
	,"content_data" varchar(4000)
	,"status" varchar(200) NOT NULL
	,"source" varchar(200) NOT NULL
	,"date_created" timestamp NOT NULL
	,"date_reminder" timestamp
	,"is_snoozable" int NOT NULL
	,"layout" varchar(20) NOT NULL
	,"image" varchar(200)
	,"is_deleted" int NOT NULL
	,"icon" varchar(200) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_admin_notes');
drop foreign table if exists wordpress_wp_wc_category_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_category_lookup("category_tree_id" bigint NOT NULL
	,"category_id" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_category_lookup');
drop foreign table if exists wordpress_wp_wc_customer_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_customer_lookup("customer_id" bigint NOT NULL
	,"user_id" bigint
	,"username" varchar(60) NOT NULL
	,"first_name" varchar(255) NOT NULL
	,"last_name" varchar(255) NOT NULL
	,"email" varchar(100)
	,"date_last_active" timestamp
	,"date_registered" timestamp
	,"country" char(2) NOT NULL
	,"postcode" varchar(20) NOT NULL
	,"city" varchar(100) NOT NULL
	,"state" varchar(100) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_customer_lookup');
drop foreign table if exists wordpress_wp_wc_download_log CASCADE; create FOREIGN TABLE wordpress_wp_wc_download_log("download_log_id" bigint NOT NULL
	,"timestamp" timestamp NOT NULL
	,"permission_id" bigint NOT NULL
	,"user_id" bigint
	,"user_ip_address" varchar(100))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_download_log');
drop foreign table if exists wordpress_wp_wc_order_coupon_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_order_coupon_lookup("order_id" bigint NOT NULL
	,"coupon_id" bigint NOT NULL
	,"date_created" timestamp NOT NULL
	,"discount_amount" float NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_order_coupon_lookup');
drop foreign table if exists wordpress_wp_wc_order_product_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_order_product_lookup("order_item_id" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"variation_id" bigint NOT NULL
	,"customer_id" bigint
	,"date_created" timestamp NOT NULL
	,"product_qty" int NOT NULL
	,"product_net_revenue" float NOT NULL
	,"product_gross_revenue" float NOT NULL
	,"coupon_amount" float NOT NULL
	,"tax_amount" float NOT NULL
	,"shipping_amount" float NOT NULL
	,"shipping_tax_amount" float NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_order_product_lookup');
drop foreign table if exists wordpress_wp_wc_order_stats CASCADE; create FOREIGN TABLE wordpress_wp_wc_order_stats("order_id" bigint NOT NULL
	,"parent_id" bigint NOT NULL
	,"date_created" timestamp NOT NULL
	,"date_created_gmt" timestamp NOT NULL
	,"num_items_sold" int NOT NULL
	,"total_sales" float NOT NULL
	,"tax_total" float NOT NULL
	,"shipping_total" float NOT NULL
	,"net_total" float NOT NULL
	,"returning_customer" int
	,"status" varchar(200) NOT NULL
	,"customer_id" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_order_stats');
drop foreign table if exists wordpress_wp_wc_order_tax_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_order_tax_lookup("order_id" bigint NOT NULL
	,"tax_rate_id" bigint NOT NULL
	,"date_created" timestamp NOT NULL
	,"shipping_tax" float NOT NULL
	,"order_tax" float NOT NULL
	,"total_tax" float NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_order_tax_lookup');
drop foreign table if exists wordpress_wp_wc_product_meta_lookup CASCADE; create FOREIGN TABLE wordpress_wp_wc_product_meta_lookup("product_id" bigint NOT NULL
	,"sku" varchar(100)
	,"virtual" int
	,"downloadable" int
	,"min_price" decimal(19,4)
	,"max_price" decimal(19,4)
	,"onsale" int
	,"stock_quantity" float
	,"stock_status" varchar(100)
	,"rating_count" bigint
	,"average_rating" decimal(3,2)
	,"total_sales" bigint
	,"tax_status" varchar(100)
	,"tax_class" varchar(100))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_product_meta_lookup');
drop foreign table if exists wordpress_wp_wc_reserved_stock CASCADE; create FOREIGN TABLE wordpress_wp_wc_reserved_stock("order_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"stock_quantity" float NOT NULL
	,"timestamp" timestamp NOT NULL
	,"expires" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_reserved_stock');
drop foreign table if exists wordpress_wp_wc_tax_rate_classes CASCADE; create FOREIGN TABLE wordpress_wp_wc_tax_rate_classes("tax_rate_class_id" bigint NOT NULL
	,"name" varchar(200) NOT NULL
	,"slug" varchar(200) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_tax_rate_classes');
drop foreign table if exists wordpress_wp_wc_webhooks CASCADE; create FOREIGN TABLE wordpress_wp_wc_webhooks("webhook_id" bigint NOT NULL
	,"status" varchar(200) NOT NULL
	,"name" text NOT NULL
	,"user_id" bigint NOT NULL
	,"delivery_url" text NOT NULL
	,"secret" text NOT NULL
	,"topic" varchar(200) NOT NULL
	,"date_created" timestamp NOT NULL
	,"date_created_gmt" timestamp NOT NULL
	,"date_modified" timestamp NOT NULL
	,"date_modified_gmt" timestamp NOT NULL
	,"api_version" smallint NOT NULL
	,"failure_count" smallint NOT NULL
	,"pending_delivery" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wc_webhooks');
drop foreign table if exists wordpress_wp_wcfm_daily_analysis CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_daily_analysis("ID" bigint NOT NULL
	,"is_shop" int NOT NULL
	,"is_store" int NOT NULL
	,"is_product" int NOT NULL
	,"product_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"count" bigint NOT NULL
	,"visited" date NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_daily_analysis');
drop foreign table if exists wordpress_wp_wcfm_detailed_analysis CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_detailed_analysis("ID" bigint NOT NULL
	,"is_shop" int NOT NULL
	,"is_store" int NOT NULL
	,"is_product" int NOT NULL
	,"product_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"referer" text NOT NULL
	,"ip_address" varchar(60) NOT NULL
	,"country" varchar(30) NOT NULL
	,"state" varchar(30) NOT NULL
	,"city" varchar(100) NOT NULL
	,"visited" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_detailed_analysis');
drop foreign table if exists wordpress_wp_wcfm_enquiries CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_enquiries("ID" bigint NOT NULL
	,"enquiry" varchar(4000) NOT NULL
	,"reply" varchar(4000) NOT NULL
	,"product_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"customer_id" bigint NOT NULL
	,"customer_name" varchar(200) NOT NULL
	,"customer_email" varchar(200) NOT NULL
	,"reply_by" bigint NOT NULL
	,"is_private" int NOT NULL
	,"posted" timestamp NOT NULL
	,"replied" date NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_enquiries');
drop foreign table if exists wordpress_wp_wcfm_enquiries_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_enquiries_meta("ID" bigint NOT NULL
	,"enquiry_id" bigint NOT NULL
	,"key" varchar(200) NOT NULL
	,"value" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_enquiries_meta');
drop foreign table if exists wordpress_wp_wcfm_enquiries_response CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_enquiries_response("ID" bigint NOT NULL
	,"enquiry_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"customer_id" bigint NOT NULL
	,"customer_name" varchar(200) NOT NULL
	,"customer_email" varchar(200) NOT NULL
	,"reply" varchar(4000) NOT NULL
	,"reply_by" bigint NOT NULL
	,"posted" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_enquiries_response');
drop foreign table if exists wordpress_wp_wcfm_enquiries_response_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_enquiries_response_meta("ID" bigint NOT NULL
	,"enquiry_response_id" bigint NOT NULL
	,"key" varchar(200) NOT NULL
	,"value" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_enquiries_response_meta');
drop foreign table if exists wordpress_wp_wcfm_fbc_chat_rows CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_fbc_chat_rows("message_id" varchar(30) NOT NULL
	,"conversation_id" varchar(30) NOT NULL
	,"user_id" varchar(30) NOT NULL
	,"user_name" varchar(32)
	,"msg" text NOT NULL
	,"msg_time" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_fbc_chat_rows');
drop foreign table if exists wordpress_wp_wcfm_fbc_chat_sessions CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_fbc_chat_sessions("conversation_id" varchar(30) NOT NULL
	,"user_id" varchar(30) NOT NULL
	,"evaluation" varchar(30) NOT NULL
	,"created_at" bigint NOT NULL
	,"duration" varchar(30) NOT NULL
	,"receive_copy" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_fbc_chat_sessions');
drop foreign table if exists wordpress_wp_wcfm_fbc_chat_visitors CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_fbc_chat_visitors("user_id" varchar(30) NOT NULL
	,"user_type" varchar(12) NOT NULL
	,"user_name" varchar(32)
	,"user_ip" int
	,"user_email" varchar(90)
	,"last_online" bigint
	,"vendor_id" varchar(30) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_fbc_chat_visitors');
drop foreign table if exists wordpress_wp_wcfm_fbc_offline_messages CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_fbc_offline_messages("id" int NOT NULL
	,"user_name" varchar(4000) NOT NULL
	,"user_email" varchar(4000) NOT NULL
	,"user_message" varchar(4000) NOT NULL
	,"user_info" varchar(4000) NOT NULL
	,"mail_date" date NOT NULL
	,"mail_read" int NOT NULL
	,"vendor_id" varchar(30) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_fbc_offline_messages');
drop foreign table if exists wordpress_wp_wcfm_following_followers CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_following_followers("ID" bigint NOT NULL
	,"user_id" bigint NOT NULL
	,"user_name" varchar(200) NOT NULL
	,"user_email" varchar(200) NOT NULL
	,"follower_id" bigint NOT NULL
	,"follower_name" varchar(200) NOT NULL
	,"follower_email" varchar(200) NOT NULL
	,"notify" int NOT NULL
	,"posted" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_following_followers');
drop foreign table if exists wordpress_wp_wcfm_marketplace_orders CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_orders("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"customer_id" bigint NOT NULL
	,"payment_method" varchar(255) NOT NULL
	,"product_id" bigint NOT NULL
	,"variation_id" bigint NOT NULL
	,"quantity" bigint NOT NULL
	,"product_price" varchar(255)
	,"purchase_price" varchar(20) NOT NULL
	,"item_id" bigint NOT NULL
	,"item_type" varchar(255)
	,"item_sub_total" varchar(255)
	,"item_total" varchar(255)
	,"shipping" varchar(255) NOT NULL
	,"tax" varchar(255) NOT NULL
	,"shipping_tax_amount" varchar(255) NOT NULL
	,"commission_amount" varchar(255) NOT NULL
	,"discount_amount" varchar(255) NOT NULL
	,"discount_type" varchar(255) NOT NULL
	,"other_amount" varchar(255) NOT NULL
	,"other_amount_type" varchar(255) NOT NULL
	,"withdrawal_id" bigint NOT NULL
	,"refunded_id" bigint NOT NULL
	,"refunded_amount" varchar(255) NOT NULL
	,"withdraw_charges" varchar(255) NOT NULL
	,"total_commission" varchar(255) NOT NULL
	,"order_status" varchar(255) NOT NULL
	,"shipping_status" varchar(255) NOT NULL
	,"commission_status" varchar(100) NOT NULL
	,"withdraw_status" varchar(100) NOT NULL
	,"refund_status" varchar(100) NOT NULL
	,"is_refunded" int NOT NULL
	,"is_partially_refunded" int NOT NULL
	,"is_withdrawable" int NOT NULL
	,"is_auto_withdrawal" int NOT NULL
	,"is_trashed" int NOT NULL
	,"commission_paid_date" timestamp
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_orders');
drop foreign table if exists wordpress_wp_wcfm_marketplace_orders_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_orders_meta("ID" bigint NOT NULL
	,"order_commission_id" bigint NOT NULL
	,"key" varchar(200) NOT NULL
	,"value" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_orders_meta');
drop foreign table if exists wordpress_wp_wcfm_marketplace_product_multivendor CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_product_multivendor("ID" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"parent_product_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"product_price" varchar(100) NOT NULL
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_product_multivendor');
drop foreign table if exists wordpress_wp_wcfm_marketplace_refund_request CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_refund_request("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"requested_by" bigint NOT NULL
	,"approved_by" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"commission_id" bigint NOT NULL
	,"item_id" bigint NOT NULL
	,"refunded_amount" varchar(255) NOT NULL
	,"refund_status" varchar(100) NOT NULL
	,"refund_reason" varchar(4000) NOT NULL
	,"is_partially_refunded" int NOT NULL
	,"refund_paid_date" timestamp
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_refund_request');
drop foreign table if exists wordpress_wp_wcfm_marketplace_refund_request_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_refund_request_meta("ID" bigint NOT NULL
	,"refund_id" bigint NOT NULL
	,"key" varchar(255) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_refund_request_meta');
drop foreign table if exists wordpress_wp_wcfm_marketplace_reverse_withdrawal CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_reverse_withdrawal("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"commission_id" bigint NOT NULL
	,"payment_method" varchar(255) NOT NULL
	,"gross_total" varchar(255) NOT NULL
	,"commission" varchar(255) NOT NULL
	,"balance" varchar(255) NOT NULL
	,"withdraw_status" varchar(100) NOT NULL
	,"withdraw_note" varchar(4000) NOT NULL
	,"withdraw_paid_date" timestamp
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_reverse_withdrawal');
drop foreign table if exists wordpress_wp_wcfm_marketplace_reverse_withdrawal_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_reverse_withdrawal_meta("ID" bigint NOT NULL
	,"reverse_withdraw_id" bigint NOT NULL
	,"key" varchar(255) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_reverse_withdrawal_meta');
drop foreign table if exists wordpress_wp_wcfm_marketplace_review_rating_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_review_rating_meta("ID" bigint NOT NULL
	,"review_id" bigint NOT NULL
	,"key" varchar(255) NOT NULL
	,"value" varchar(255) NOT NULL
	,"type" varchar(200) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_review_rating_meta');
drop foreign table if exists wordpress_wp_wcfm_marketplace_reviews CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_reviews("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"author_name" varchar(255) NOT NULL
	,"author_email" varchar(255) NOT NULL
	,"review_title" varchar(4000) NOT NULL
	,"review_description" varchar(4000) NOT NULL
	,"review_rating" varchar(100) NOT NULL
	,"approved" int NOT NULL
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_reviews');
drop foreign table if exists wordpress_wp_wcfm_marketplace_reviews_response CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_reviews_response("ID" bigint NOT NULL
	,"review_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"author_name" varchar(255) NOT NULL
	,"author_email" varchar(255) NOT NULL
	,"reply" varchar(4000) NOT NULL
	,"reply_by" bigint NOT NULL
	,"posted" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_reviews_response');
drop foreign table if exists wordpress_wp_wcfm_marketplace_reviews_response_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_reviews_response_meta("ID" bigint NOT NULL
	,"review_response_id" bigint NOT NULL
	,"key" varchar(255) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_reviews_response_meta');
drop foreign table if exists wordpress_wp_wcfm_marketplace_shipping_zone_locations CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_shipping_zone_locations("id" int NOT NULL
	,"vendor_id" int
	,"zone_id" int
	,"location_code" varchar(255)
	,"location_type" varchar(255))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_shipping_zone_locations');
drop foreign table if exists wordpress_wp_wcfm_marketplace_shipping_zone_methods CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_shipping_zone_methods("instance_id" int NOT NULL
	,"method_id" varchar(255) NOT NULL
	,"zone_id" int NOT NULL
	,"vendor_id" int NOT NULL
	,"is_enabled" int NOT NULL
	,"settings" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_shipping_zone_methods');
drop foreign table if exists wordpress_wp_wcfm_marketplace_store_taxonomies CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_store_taxonomies("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"term" bigint NOT NULL
	,"parent" bigint
	,"taxonomy" varchar(100) NOT NULL
	,"lang" varchar(20))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_store_taxonomies');
drop foreign table if exists wordpress_wp_wcfm_marketplace_vendor_ledger CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_vendor_ledger("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"credit" varchar(100) NOT NULL
	,"debit" varchar(100) NOT NULL
	,"reference_id" bigint NOT NULL
	,"reference" varchar(100) NOT NULL
	,"reference_details" text NOT NULL
	,"reference_status" varchar(100) NOT NULL
	,"reference_update_date" timestamp
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_vendor_ledger');
drop foreign table if exists wordpress_wp_wcfm_marketplace_withdraw_request CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_withdraw_request("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"order_ids" varchar(4000) NOT NULL
	,"commission_ids" varchar(4000) NOT NULL
	,"payment_method" varchar(255) NOT NULL
	,"withdraw_amount" varchar(255) NOT NULL
	,"withdraw_charges" varchar(255) NOT NULL
	,"withdraw_status" varchar(100) NOT NULL
	,"withdraw_mode" varchar(100) NOT NULL
	,"withdraw_note" varchar(4000) NOT NULL
	,"is_auto_withdrawal" int NOT NULL
	,"withdraw_paid_date" timestamp
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_withdraw_request');
drop foreign table if exists wordpress_wp_wcfm_marketplace_withdraw_request_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_marketplace_withdraw_request_meta("ID" bigint NOT NULL
	,"withdraw_id" bigint NOT NULL
	,"key" varchar(255) NOT NULL
	,"value" varchar(255) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_marketplace_withdraw_request_meta');
drop foreign table if exists wordpress_wp_wcfm_membership_subscription CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_membership_subscription("ID" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"membership_id" bigint NOT NULL
	,"subscription_type" varchar(50) NOT NULL
	,"subscription_amt" int NOT NULL
	,"subscription_interval" varchar(50) NOT NULL
	,"event" varchar(50) NOT NULL
	,"pay_mode" varchar(50) NOT NULL
	,"transaction_id" varchar(100) NOT NULL
	,"transaction_type" varchar(100) NOT NULL
	,"transaction_status" varchar(100) NOT NULL
	,"transaction_details" text NOT NULL
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_membership_subscription');
drop foreign table if exists wordpress_wp_wcfm_messages CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_messages("ID" bigint NOT NULL
	,"message" varchar(4000) NOT NULL
	,"author_id" bigint NOT NULL
	,"reply_to" bigint NOT NULL
	,"message_to" bigint NOT NULL
	,"author_is_admin" int NOT NULL
	,"author_is_vendor" int NOT NULL
	,"author_is_customer" int NOT NULL
	,"is_notice" int NOT NULL
	,"is_direct_message" int NOT NULL
	,"is_pined" int NOT NULL
	,"message_type" varchar(100) NOT NULL
	,"created" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_messages');
drop foreign table if exists wordpress_wp_wcfm_messages_modifier CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_messages_modifier("ID" bigint NOT NULL
	,"message" bigint NOT NULL
	,"is_read" int NOT NULL
	,"read_by" bigint NOT NULL
	,"read_on" timestamp NOT NULL
	,"is_trashed" int NOT NULL
	,"trashed_by" bigint NOT NULL
	,"trashed_on" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_messages_modifier');
drop foreign table if exists wordpress_wp_wcfm_messages_stat CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_messages_stat("ID" bigint NOT NULL
	,"message" bigint NOT NULL
	,"is_liked" int NOT NULL
	,"liked_by" bigint NOT NULL
	,"liked_on" timestamp NOT NULL
	,"is_disliked" int NOT NULL
	,"disliked_by" bigint NOT NULL
	,"disliked_on" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_messages_stat');
drop foreign table if exists wordpress_wp_wcfm_support CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_support("ID" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"item_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"author_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"customer_id" bigint NOT NULL
	,"customer_name" varchar(200) NOT NULL
	,"customer_email" varchar(200) NOT NULL
	,"query" varchar(4000) NOT NULL
	,"category" varchar(4000) NOT NULL
	,"priority" varchar(200) NOT NULL
	,"status" varchar(200) NOT NULL
	,"posted" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_support');
drop foreign table if exists wordpress_wp_wcfm_support_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_support_meta("ID" bigint NOT NULL
	,"support_id" bigint NOT NULL
	,"key" varchar(200) NOT NULL
	,"value" varchar(4000) NOT NULL
	,"type" varchar(200) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_support_meta');
drop foreign table if exists wordpress_wp_wcfm_support_response CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_support_response("ID" bigint NOT NULL
	,"support_id" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"item_id" bigint NOT NULL
	,"product_id" bigint NOT NULL
	,"vendor_id" bigint NOT NULL
	,"customer_id" bigint NOT NULL
	,"reply" varchar(4000) NOT NULL
	,"reply_by" bigint NOT NULL
	,"posted" timestamp NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_support_response');
drop foreign table if exists wordpress_wp_wcfm_support_response_meta CASCADE; create FOREIGN TABLE wordpress_wp_wcfm_support_response_meta("ID" bigint NOT NULL
	,"support_response_id" bigint NOT NULL
	,"key" varchar(200) NOT NULL
	,"value" varchar(4000) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_wcfm_support_response_meta');
drop foreign table if exists wordpress_wp_woocommerce_api_keys CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_api_keys("key_id" bigint NOT NULL
	,"user_id" bigint NOT NULL
	,"description" varchar(200)
	,"permissions" varchar(10) NOT NULL
	,"consumer_key" char(64) NOT NULL
	,"consumer_secret" char(43) NOT NULL
	,"nonces" varchar(4000)
	,"truncated_key" char(7) NOT NULL
	,"last_access" timestamp)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_api_keys');
drop foreign table if exists wordpress_wp_woocommerce_attribute_taxonomies CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_attribute_taxonomies("attribute_id" bigint NOT NULL
	,"attribute_name" varchar(200) NOT NULL
	,"attribute_label" varchar(200)
	,"attribute_type" varchar(20) NOT NULL
	,"attribute_orderby" varchar(20) NOT NULL
	,"attribute_public" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_attribute_taxonomies');
drop foreign table if exists wordpress_wp_woocommerce_downloadable_product_permissions CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_downloadable_product_permissions("permission_id" bigint NOT NULL
	,"download_id" varchar(36) NOT NULL
	,"product_id" bigint NOT NULL
	,"order_id" bigint NOT NULL
	,"order_key" varchar(200) NOT NULL
	,"user_email" varchar(200) NOT NULL
	,"user_id" bigint
	,"downloads_remaining" varchar(9)
	,"access_granted" timestamp NOT NULL
	,"access_expires" timestamp
	,"download_count" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_downloadable_product_permissions');
drop foreign table if exists wordpress_wp_woocommerce_log CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_log("log_id" bigint NOT NULL
	,"timestamp" timestamp NOT NULL
	,"level" smallint NOT NULL
	,"source" varchar(200) NOT NULL
	,"message" varchar(4000) NOT NULL
	,"context" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_log');
drop foreign table if exists wordpress_wp_woocommerce_order_itemmeta CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_order_itemmeta("meta_id" bigint NOT NULL
	,"order_item_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_order_itemmeta');
drop foreign table if exists wordpress_wp_woocommerce_order_items CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_order_items("order_item_id" bigint NOT NULL
	,"order_item_name" text NOT NULL
	,"order_item_type" varchar(200) NOT NULL
	,"order_id" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_order_items');
drop foreign table if exists wordpress_wp_woocommerce_payment_tokenmeta CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_payment_tokenmeta("meta_id" bigint NOT NULL
	,"payment_token_id" bigint NOT NULL
	,"meta_key" varchar(255)
	,"meta_value" varchar(4000))SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_payment_tokenmeta');
drop foreign table if exists wordpress_wp_woocommerce_payment_tokens CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_payment_tokens("token_id" bigint NOT NULL
	,"gateway_id" varchar(200) NOT NULL
	,"token" text NOT NULL
	,"user_id" bigint NOT NULL
	,"type" varchar(200) NOT NULL
	,"is_default" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_payment_tokens');
drop foreign table if exists wordpress_wp_woocommerce_sessions CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_sessions("session_id" bigint NOT NULL
	,"session_key" char(32) NOT NULL
	,"session_value" varchar(4000) NOT NULL
	,"session_expiry" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_sessions');
drop foreign table if exists wordpress_wp_woocommerce_shipping_zone_locations CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_shipping_zone_locations("location_id" bigint NOT NULL
	,"zone_id" bigint NOT NULL
	,"location_code" varchar(200) NOT NULL
	,"location_type" varchar(40) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_shipping_zone_locations');
drop foreign table if exists wordpress_wp_woocommerce_shipping_zone_methods CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_shipping_zone_methods("zone_id" bigint NOT NULL
	,"instance_id" bigint NOT NULL
	,"method_id" varchar(200) NOT NULL
	,"method_order" bigint NOT NULL
	,"is_enabled" int NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_shipping_zone_methods');
drop foreign table if exists wordpress_wp_woocommerce_shipping_zones CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_shipping_zones("zone_id" bigint NOT NULL
	,"zone_name" varchar(200) NOT NULL
	,"zone_order" bigint NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_shipping_zones');
drop foreign table if exists wordpress_wp_woocommerce_tax_rate_locations CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_tax_rate_locations("location_id" bigint NOT NULL
	,"location_code" varchar(200) NOT NULL
	,"tax_rate_id" bigint NOT NULL
	,"location_type" varchar(40) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_tax_rate_locations');
drop foreign table if exists wordpress_wp_woocommerce_tax_rates CASCADE; create FOREIGN TABLE wordpress_wp_woocommerce_tax_rates("tax_rate_id" bigint NOT NULL
	,"tax_rate_country" varchar(2) NOT NULL
	,"tax_rate_state" varchar(200) NOT NULL
	,"tax_rate" varchar(8) NOT NULL
	,"tax_rate_name" varchar(200) NOT NULL
	,"tax_rate_priority" bigint NOT NULL
	,"tax_rate_compound" int NOT NULL
	,"tax_rate_shipping" int NOT NULL
	,"tax_rate_order" bigint NOT NULL
	,"tax_rate_class" varchar(200) NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woocommerce_tax_rates');
drop foreign table if exists wordpress_wp_woof_query_cache CASCADE; create FOREIGN TABLE wordpress_wp_woof_query_cache("mkey" varchar(64) NOT NULL
	,"mvalue" text NOT NULL)SERVER mysql_fdw_wordpress OPTIONS (dbname 'wordpress', table_name 'wp_woof_query_cache');
