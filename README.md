
# BI Som Connexió Ansible inventory

This repository stores hosts informations and related variables for this specific instance of BI infrastructure.

## Requirements

1. Clone this repo and [bi-provisioning](https://gitlab.com/coopdevs/bi-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd odoo-somconnexio-inventory
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to `bi-provisioning` directory and install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # use the user root the first time to create the other users: --user=root
   ansible-playbook playbooks/sys_admins.yml -i ../bi-opcions-inventory/inventory/hosts --limit=dev
   ansible-playbook playbooks/provision.yml -i ../bi-opcions-inventory/inventory/hosts --ask-vault-pass --limit=dev
   ```
   BW key: BI SC provisioning secret dev

   * Production mode
   ```sh
   ansible-playbook playbooks/sys_admins.yml -i ../bi-opcions-inventory/inventory/hosts --ask-vault-pass --limit=production
   ansible-playbook playbooks/provision.yml -i ../bi-opcions-inventory/inventory/hosts --ask-vault-pass --limit=production
   ```
   BW key: BI SC provisioning secret



## Database configurations

Open the file inventory/host_bars/<environment>/db_configurations.yml

Complete the follwing collections:

**fdw_script_files**
*It contains the scripts name inside the inventory/group_bars folder with the creation foreign tables scripts.*
Eg.
  fdw_script_files:
    - name: fdw_odoo.sql
    - name: fdw_opencell.sql
    - name: fdw_otrs.sql

**fdw_connections_nossl**
*It contains the databases information for databases without ssl certificates, but database information.*
*You must introduce theese fields:*

       - name: connection name (eg. odoo)
       - extension: foreign data wrapper extension (eg. postgres_fdw)
       -  user: database user
       -  password: database password
       -  host: server host
       -  port: server port
       -  dbname: database name

**fdw_connections_ssl**
*It contains the databases information for databases with ssl certificates.*
*You must introduce theese fields:*

       - name: connection name (eg. otrs)
       - extension: foreign data wrapper extension (eg. mysql_fdw)
       -  user: database user
       -  password: database password
       -  host: server host
       -  port: server port
       -  sslkey: ssl key filename
       -  sslcert: certificate file name
       -  sslca: ca filename


**fdw_connections_nossl_nodb**
*It contains the databases information for databases without ssl certificates and withoyt database information.*
*You must introduce theese fields:*

       - name: connection name (eg. otrs)
       - extension: foreign data wrapper extension (eg. mysql_fdw)
       -  user: database user
       -  password: database password
       -  host: server host
       -  port: server port

**ssl_files**
*It contains the data inside the ssl files*
*You must introduce theese fields:*

       - name: file name (eg. comany-ca-cert.pem)
       - value: file content

**fdw_csv**
*It contains the foreign data wrapper tables for CSV files*
*You must introduce theese fields:*

       - name: table name
       - value: table columns (eg. a int, b varchar(50) )

